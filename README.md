# Grpuavpny Punyyratr

gy;qe Vs lbh guvax guvf jubyr guvat vf fghcvq, naq lbh nyernql unir fghss lbh jebgr gb fubj
sbe lbhefrys, lbh pna fxvc guvf jubyr guvat. Xrrc va zvaq gung jr jnag
gb frr gung 1. lbh pna pbqr 2. va Clguba 3. fbzrguvat nabgure uhzna orvat pna
haqrefgnaq 4. gung pna or hfrq va cebqhpgvba.

Vs lbh guvax vg'f abg fghcvq, ohg lbh'q yvxr gb xabj zber nobhg gur cbfvgvba,
jung jr bssre naq jung gb rkcrpg orsber lbh rzonex ba gur gnfx, gung'f cresrpgyl ernfbanoyr, fubbg hf na rznvy.

Nyy pbzzhavpngvbaf naq fhozvffvbaf gb or frag gb zryvffn@bzavfgernz.pb.

## Fpbcr

Jevgr n fvzcyr Clguba zbqhyr gb fpencr qngn bhg bs uggcf://jjj.jhaqretebhaq.pbz/. Rknzcyr hfntr:

```clguba
sebz jh vzcbeg trg_uvfgbevpny_qngn
vzcbeg qngrgvzr

trg_uvfgbevpny_qngn('ft/fvatncber', qngrgvzr.qngr(2018, 11, 1), qngrgvzr.qngr(2018, 11, 4))
```

naq gur bhgchg:

```
[{'nitbxgnf': 6,
  'pbbyvatqrterrqnlf': 15,
  'qngr': qngrgvzr.qngr(2018, 11, 1),
  'qrjcbvag': 24,
  'tqrterrqnlf': 30,
  'znk_qrjcbvag': 25,
  'znk_uhzvqvgl': 94,
  'znk_cerffher': 1010,
  'znk_grzcrengher': 28,
  'znk_grzcrengher_erpbeq': 33,
  'znk_grzcrengher_erpbeq_lrne': 2005,
  'znk_ivfvovyvgl': 10,
  'znk_jvaq_fcrrq': 23,
  'zva_qrjcbvag': 23,
  'zva_uhzvqvgl': 79,
  'zva_cerffher': 1007,
  'zva_grzcrengher': 26,
  'zva_grzcrengher_erpbeq': 22,
  'zva_grzcrengher_erpbeq_lrne': 2008,
  'zva_ivfvovyvgl': 8,
  'zva_jvaq_fcrrq': 0,
  'cerpvcerpbeqlrne': 2018,
  'cerffher': 1009,
  'grzcrengher': 27,
  'ivfvovyvgl': 10,
  'jvaq_fcrrq': 5},
 {'nitbxgnf': 5,
  'pbbyvatqrterrqnlf': 18,
  'qngr': qngrgvzr.qngr(2018, 11, 2),
  'qrjcbvag': 24,
  'tqrterrqnlf': 34,
  'znk_qrjcbvag': 26,
  'znk_uhzvqvgl': 100,
  'znk_cerffher': 1012,
  'znk_grzcrengher': 32,
  'znk_grzcrengher_erpbeq': 33,
  'znk_grzcrengher_erpbeq_lrne': 2012,
  'znk_ivfvovyvgl': 10,
  'znk_jvaq_fcrrq': 21,
  'zva_qrjcbvag': 22,
  'zva_uhzvqvgl': 59,
  'zva_cerffher': 1008,
  'zva_grzcrengher': 26,
  'zva_grzcrengher_erpbeq': 23,
  'zva_grzcrengher_erpbeq_lrne': 1999,
  'zva_ivfvovyvgl': 5,
  'zva_jvaq_fcrrq': 0,
  'cerpvcerpbeqlrne': 2018,
  'cerffher': 1009,
  'grzcrengher': 29,
  'ivfvovyvgl': 9,
  'jvaq_fcrrq': 8},
 {'nitbxgnf': 4,
  'pbbyvatqrterrqnlf': 18,
  'qngr': qngrgvzr.qngr(2018, 11, 3),
  'qrjcbvag': 25,
  'tqrterrqnlf': 34,
  'znk_qrjcbvag': 26,
  'znk_uhzvqvgl': 94,
  'znk_cerffher': 1012,
  'znk_grzcrengher': 32,
  'znk_grzcrengher_erpbeq': 33,
  'znk_grzcrengher_erpbeq_lrne': 2003,
  'znk_ivfvovyvgl': 10,
  'znk_jvaq_fcrrq': 24,
  'zva_qrjcbvag': 24,
  'zva_uhzvqvgl': 66,
  'zva_cerffher': 1008,
  'zva_grzcrengher': 26,
  'zva_grzcrengher_erpbeq': 23,
  'zva_grzcrengher_erpbeq_lrne': 2001,
  'zva_ivfvovyvgl': 4,
  'zva_jvaq_fcrrq': 0,
  'cerpvcerpbeqlrne': 2018,
  'cerffher': 1010,
  'grzcrengher': 29,
  'ivfvovyvgl': 9,
  'jvaq_fcrrq': 5},
 {'nitbxgnf': 5,
  'pbbyvatqrterrqnlf': 20,
  'qngr': qngrgvzr.qngr(2018, 11, 4),
  'qrjcbvag': 26,
  'tqrterrqnlf': 34,
  'znk_qrjcbvag': 27,
  'znk_uhzvqvgl': 94,
  'znk_cerffher': 1012,
  'znk_grzcrengher': 33,
  'znk_grzcrengher_erpbeq': 33,
  'znk_grzcrengher_erpbeq_lrne': 2003,
  'znk_ivfvovyvgl': 10,
  'znk_jvaq_fcrrq': 16,
  'zva_qrjcbvag': 25,
  'zva_uhzvqvgl': 63,
  'zva_cerffher': 1007,
  'zva_grzcrengher': 26,
  'zva_grzcrengher_erpbeq': 22,
  'zva_grzcrengher_erpbeq_lrne': 2005,
  'zva_ivfvovyvgl': 8,
  'zva_jvaq_fcrrq': 0,
  'cerpvcerpbeqlrne': 2018,
  'cerffher': 1009,
  'grzcrengher': 29,
  'ivfvovyvgl': 10,
  'jvaq_fcrrq': 5}]
```


## Shapgvba fvtangher

- gur ybpngvba gb or fpenccrq. Ybpngvba pna or sbhaq ol tbvat gb
uggcf://jjj.jhaqretebhaq.pbz/, frnepuvat sbe n ybpngvba
(r.t.: Fvatncber), fryrpgvat gur zbfg eryrinag bar. Gur ybpngvba vf gur
ynfg cneg bs gur HEY
(`uggcf://jjj.jhaqretebhaq.pbz/jrngure/`**ft/fvatncber**). Jr rkcrpg
hfre bs gur shapgvba gb cebivqr n cebcre ybpngvba, frnepuvat sbe vg
vf bhg bs gur fpbcr bs guvf cebwrpg.
- gur ortva naq raq qngr gb ybbx sbe.

## Bhgchg

N yvfg bs qvpg, pbagnvavat gur ergevrirq svryqf sbe rnpu qnl jvguva gur qngr vagreiny.

## Nalguvat ryfr?

Lbh ZHFG hfr Clguba 3+, lbh pna hfr nal irefvba nsgre gung (rira gur yngrfg bar).
Pbqr ZHFG ernqvyl vzcbeg-noyr nf va gur nobir rknzcyr.

Lbh PNA ABG hfr nal "urnqyrff oebjfre", fhpu nf Fryravhz. Gur bayl aba-pber
zbqhyr lbh ner nyybjrq (ohg abg sbeprq) gb hfr ner:
- [erdhrfgf](uggc://qbpf.clguba-erdhrfgf.bet/ra/znfgre/);
- [ykzy](uggcf://ykzy.qr/);
- [OrnhgvshyFbhc](uggcf://jjj.pehzzl.pbz/fbsgjner/OrnhgvshyFbhc/of4/qbp/).

## Rinyhngvba

Lbh jvyy abgnoyl or rinyhngrq ba gur sbyybjvat cbvagf:

- dhnyvgl bs gur pbqr naq bs gur fbyhgvba. Jr fgebatyl, fgebatyl hetr lbh gb ernq uggcf://jjj.clguba.bet/qri/crcf/crc-0008/ vs lbh arire unir.
- reebe naq harkcrpgrq pnfr unaqyvat
- pynevgl bs gur pbqr naq vgf qbphzragngvba

Erzrzore jung jr'er gelvat gb zrnfher urer: 1. lbh pna pbqr 2. va Clguba 3. fbzrguvat nabgure uhzna orvat pna haqrefgnaq 4. gung pna or hfrq va cebqhpgvba.

Jr qba'g rkcrpg lbh gb fcraq zber guna n pbhcyr ubhef ba guvf.
Xrrc genpx bs ubj zhpu gvzr lbh fcrag naq fraq lbhe fbyhgvba jura lbh'er ernql.
Nalguvat lbh qvqa'g unir gvzr gb vzcyrzrag, ohg pbafvqrerq, qb funer jvgu hf
(be trg ernql gb qrsraq qhevat gur vagreivrj). Gur cresrpg, sbby cebbs
fbyhgvba vf abg rkcrpgrq. Ba gur bgure unaq, vs lbh ner hanoyr gb trg
nalguvat jvguva gur svefg ubhe jbexvat ba gur ceboyrz, lbh ner cebonoyl
abg svg sbe gur cbfvgvba: trg orggre naq gel ntnva jura lbh tnva zber rkcrevrapr!

Lbh ner rapbhentrq gb jevgr n fubeg fhzznel nobhg lbhe fbyhgvba naq ubj
zhpu gvzr vg gbbx lbh, frcnengryl, abgnoyl gb rkcynva jung rqtr pnfr lbh
rapbhagrerq, jung lbh jbhyq unir qbar tvira zber gvzr, nal fubegpbzvatf, nal cnva cbvagf...

Nyfb, srry serr gb nggnpu lbhe erfhzr nybat jvgu lbhe fhozvffvba.
